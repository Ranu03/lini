<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()

	{	
		$this->load->model('model_slide');
		$a['slide']	=$this->model_slide->tampil(); 
		$a['utama']	=$this->model_slide->utama(); 
		$a['datamitra']	=$this->model_slide->mitra(); 
		$this->load->model('userAdmin');
		$a['data']	=$this->userAdmin->test(); 
		
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();

		$a['content'] = 'content/home';
		$this->load->view('index',$a);
	}
	public function cabang()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/cabang';
		$this->load->view('index',$a);
	}
	
	public function index_eng()
	{	
		$this->load->model('model_slide');
		$a['slide']	=$this->model_slide->tampil(); 
		$a['utama']	=$this->model_slide->utama(); 
		$a['datamitra']	=$this->model_slide->mitra(); 

		$this->load->model('userAdmin');
		$a['data']	=$this->userAdmin->test(); 

		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/home_eng';
		$this->load->view('index_eng',$a);
	}

	public function solusi()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi';
		$this->load->view('index',$a);
	}
	
	public function solusi_eng()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi_eng';
		$this->load->view('index_eng',$a);
	}
	
	public function solusi_b()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi_b';
		$this->load->view('index',$a);
	}

	public function solusi_b_eng()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi_b_eng';
		$this->load->view('index_eng',$a);
	}
	
	public function solusi_d()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi_d';
		$this->load->view('index',$a);
	}
	
	public function solusi_d_eng()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/solusi_d_eng';
		$this->load->view('index_eng',$a);
	}
	
	// about us
	public function tentang()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/tentang';
		$this->load->view('index',$a);
	}
	public function about(){
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/about';
		$this->load->view('index_eng',$a);
	}
	public function album()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/album';
		$this->load->view('index',$a);
	}
	public function galery()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/galery';
		$this->load->view('index_eng',$a);
	}
	// Q&A
	public function qa()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/qa';
		$this->load->view('index',$a);
	}

	public function qa_en()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/qa_en';
		$this->load->view('index_eng',$a);
	}
	// tele
	public function telegram()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/telegram';
		$this->load->view('index',$a);
	}
	public function tele_en()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/tele_en';
		$this->load->view('index_eng',$a);
	}
	// manual
	public function manual()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/manual';
		$this->load->view('index',$a);
	}
	// manual_eng
	public function manual_en()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/manual_eng';
		$this->load->view('index_eng',$a);
	}
	// tele
	public function video()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/video';
		$this->load->view('index',$a);
	}
	public function video_en()
	{
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['content'] = 'content/video_eng';
		$this->load->view('index_eng',$a);
	}
	// ./produk
	public function satu()
	{
		$key = $this->uri->segment(3);
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		// $a['sp'] = $this->Model_product->det_sp($key)->result();
		$a['data']      = $this->Model_product->details($key)->result();
		// $a['spesifikasi'] = $this->Model_product->sp_m($key); 
		// $isi['spesifikasi'] = $this->Model_product->spesifikasi($key)->num_rows();

		$a['content'] = 'content/produk/satu';

		$this->load->view('index',$a);
	}
	public function produk()
	{
		$key = $this->uri->segment(3);
		$this->load->model('Model_product');
		$a['produk']	= $this->Model_product->tampil();
		$a['fitur'] = $this->Model_product->fitur($key)->result();
		$a['fiturid'] = $this->Model_product->fiturid($key)->result();
		$a['fituridd'] = $this->Model_product->fituridd($key)->result();
		$a['spes0'] = $this->Model_product->spesid0($key)->result();
		$a['spes1'] = $this->Model_product->spesid1($key)->result();
		$a['spes2'] = $this->Model_product->spesid2($key)->result();
		$a['spes3'] = $this->Model_product->spesid3($key)->result();

		$a['kel'] = $this->Model_product->kelebihan($key)->result();
		$a['kell'] = $this->Model_product->kelebihan($key)->num_rows();

		$a['totalfitur'] =  $this->Model_product->fiturid($key)->num_rows();
		$a['data']      = $this->Model_product->details($key)->result();
		$a['content'] = 'content/produk/fix';
		if ($key == null) {
		    echo"
		    <script>
		    document.location='". base_url() ."';
		    </script>
		    ";

		}
		else{
		$this->load->view('index',$a);
		}
		
	}
	
}
