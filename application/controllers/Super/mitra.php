<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mitra extends CI_Controller {
	  public function __construct() {
    parent::__construct();
    $this->load->helper(array('url','html','form'));
  }


	public function index()
	{
		$this->model_scurity->getscurity();
		$isi['page'] = 'mitra';
		$isi['label'] = 'Mitra';
		$this->load->model('model_slide');
		$isi['datakks']	=$this->model_slide->mitra(); 

		$this->load->model('userAdmin');
	
		$user = $this->session->userdata('username');
		$this->load->view('backend/main',$isi);
	}



	public function simpan()
	{
		$this->model_scurity->getscurity();
		$user = $this->session->userdata('username');
		
//		
		$nmfile = $user.time(); //nama file saya beri nama langsung dan diikuti fungsi time
		$config['upload_path'] = './asset/images/slide/';
		$config['file_name'] = $nmfile; //nama yang terupload nantinya
		$config['allowed_types'] = 'gif|jpg|png|docx|zip|rar';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';
				
		$this->load->library('upload', $config);
// 

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			// $this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$gbr = $this->upload->data();
			// $this->load->view('upload_success', $data);
		}
		if(isset($_POST ['register'])) {
			$this->form_validation->set_rules('nama', 'nama', 'required');
			error_reporting(0);
			//jika validasi ok
			if ($this->form_validation->run()== true) {
				echo "<script> alert('Sukses'); </script>";

			// FORM OK
			$data = array(
				'id' =>time() ,
				'nama' =>$_POST['nama'] ,
				'deskripsi' =>$_POST['deskripsi'] ,
				 'img' => $gbr['file_name'],
				 'status' => '3',

				 );
				$this->db->insert('slide',$data);
				$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
				redirect("Admin/mitra","refresh");
			}
		}
		echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
				redirect("Admin/slide","refresh");
}
  	public function hapus()
	{	
$level = $this->session->userdata('level');
		
		if($level== 'Admin'){			
		$this->model_scurity->getscurity();
		$key = $this->uri->segment(4);

		$this->db->where('id',$key);		
		$this->db->delete('slide');
		$query = $this->db->get('slide');
		echo "<script> alert('Sukses'); </script>";
		redirect("Admin/mitra","refresh");


		}else{

		// echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
			redirect("admin/mitra","refresh");
		}	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */