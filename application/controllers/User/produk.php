<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class produk extends CI_Controller {

	public function index()
	{
		$this->model_scurity->getscurity();
		$isi['page'] = 'produk';
		$isi['label'] = 'Produk';
		$isi['datakks']	= $this->db->get('produk');
		
		$user = $this->session->userdata('username');
		$this->load->view('backend/main',$isi);
	}
	public function details_produk()
	{	
		$key = $this->uri->segment(4);
		$this->model_scurity->getscurity();
		$isi['page'] = 'details_produk';
		$isi['label'] = 'Produk';
		$this->load->model('Model_product');
		$isi['data'] = $this->Model_product->details($key)->result();
		
		
		$this->load->view('backend/main',$isi);
	}
		public function fitur()
	{	
		$key = $this->uri->segment(4);
		$this->model_scurity->getscurity();
		$isi['page'] = 'fitur';
		$isi['label'] = 'Produk';
		$this->load->model('Model_product');
		$isi['data'] = $this->Model_product->details($key)->result();
		$isi['fitur'] = $this->Model_product->fitur($key)->result();
		
		
		$this->load->view('backend/main',$isi);
	}
			public function spesifikasi()
	{	
		$key = $this->uri->segment(4);
		$this->model_scurity->getscurity();
		$isi['page'] = 'spesifikasi';
		$isi['label'] = 'Produk';
		$this->load->model('Model_product');
		$isi['data'] = $this->Model_product->details($key)->result();
		$isi['spesifikasi'] = $this->Model_product->spes($key)->result();
		
		
		$this->load->view('backend/main',$isi);
	}		
			public function kelebihan()
	{	
		$key = $this->uri->segment(4);
		$this->model_scurity->getscurity();
		$isi['page'] = 'kelebihan';
		$isi['label'] = 'Produk';
		$this->load->model('Model_product');
		$isi['data'] = $this->Model_product->details($key)->result();
		$isi['kelebihan'] = $this->Model_product->kelebihan($key)->result();
		
		
		$this->load->view('backend/main',$isi);
	}		
	public function ubah()
	{
		$this->model_scurity->getscurity();
		$user = $this->session->userdata('username');
		
//		
		$nmfile = $user.time(); //nama file saya beri nama langsung dan diikuti fungsi time
		$config['upload_path'] = './asset/images/portfolio/';
		$config['file_name'] = $nmfile; //nama yang terupload nantinya
		$config['allowed_types'] = 'gif|jpg|png|docx|zip|rar';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';
				
		$this->load->library('upload', $config);
// 

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			// $this->load->view('upload_form', $error);
		}
		else
		{
	
			$data = array('upload_data' => $this->upload->data());
			$gbr = $this->upload->data();
		
			// $this->load->view('upload_success', $data);
		}

			

			if(isset($_POST ['register'])) {
			$this->form_validation->set_rules('nama_produk', 'nama_produk', 'required');
			error_reporting(0);
			//jika validasi ok
			if ($this->form_validation->run()== true) {
				echo "<script> alert('Sukses'); </script>";
				$key =$_POST['id_registrasi'] ;
			// FORM OK
			$data = array(
				
				'nama_produk' =>$_POST['nama_produk'] ,
				'harga' =>$_POST['harga'] ,
				'deskripsi' =>$_POST['deskripsi'] ,
				 'img' => $gbr['file_name'],

				 );
				
				$this->db->where('id_produk',$key);	
				$this->db->update('produk',$data);

				$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
				redirect("admin/produk","refresh");
			}
		}
		echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
				redirect("admin/produk","refresh");
}


	public function simpan()
	{
		$this->model_scurity->getscurity();
		$user = $this->session->userdata('username');
		
//		
		$nmfile = $user.time(); //nama file saya beri nama langsung dan diikuti fungsi time
		$config['upload_path'] = './asset/images/portfolio/';
		$config['file_name'] = $nmfile; //nama yang terupload nantinya
		$config['allowed_types'] = 'gif|jpg|png|docx|zip|rar';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';
				
		$this->load->library('upload', $config);
// 

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			// $this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$gbr = $this->upload->data();
			// $this->load->view('upload_success', $data);
		}
if(isset($_POST ['register'])) {
			$this->form_validation->set_rules('nama', 'nama', 'required');
error_reporting(0);
			//jika validasi ok
			if ($this->form_validation->run()== true) {
				echo "<script> alert('Sukses'); </script>";

			// FORM OK
			$data = array(
				'id_produk' =>time() ,
				'nama_produk' =>$_POST['nama'] ,
				'harga' =>number_format($_POST['harga']) ,
				'deskripsi' =>$_POST['deskripsi'] ,
				 'img' => $gbr['file_name'],

				 );
				$this->db->insert('produk',$data);
				$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
				redirect("Admin/produk","refresh");
			}
		}
		echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
				redirect("Admin/produk","refresh");
}


	// data administrasi
	public function tambah() {
		$this->model_scurity->getscurity();
		$user = $this->session->userdata('username');
		if(isset($_POST ['register'])) {
			$a['id_produk'] = time();
			$a['nama_produk'] = preg_replace('/[^A-Za-z\  ]/', ' ', $this->input->post('nama'));
			$a['harga'] = number_format($_POST['harga']);
			$a['deskripsi'] = preg_replace('/[^A-Za-z\  ]/', ' ', $this->input->post('deskripsi'));
			$a['img'] = ($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : $this->input->post('userfile') ;
			$a['background'] = ($_FILES['bg']['name']) ? $_FILES['bg']['name'] : $this->input->post('bg') ;
			// $a['id_user'] = $this->session->userdata('id');

			// exit;

				
				
					$config['upload_path'] = './asset/images/portfolio/';
					$config['allowed_types'] = 'jpg|png|pdf';
					$config['max_size'] = '2048';
					
					$this->load->library('upload',$config);
					$upload1 = $this->upload->do_upload('userfile');
					$upload2 = $this->upload->do_upload('bg');
					
					$this->upload->data();

					if(!$upload1 && !$upload2){
						echo "<script>alert('Gagal Mengupload file atau size file terlalu besar')</script>";
						echo "<script>alert('".$this->upload->display_errors()."')</script>";
						redirect("Admin/produk","refresh");
					}else{
						$cek = $this->db->insert('produk', $a);
						if ($cek) {
							echo "<script>alert('Data berhasil disimpan')</script>";
							redirect("Admin/produk","refresh");
						}
						else{
							echo "<script>alert('Gagal Menambahkan data')</script>";
						redirect("Admin/produk","refresh");
						}
					}
				
			
				// kalau upload 2 dokumen
					if ($_FILES['userfile']['name'] != Null && $_FILES['bg']['name'] != Null) {
						$upload1 = $this->upload->do_upload('userfile');
						$upload2 = $this->upload->do_upload('bg');
						$this->upload->data();
				        
				        if (!$upload1 && !$upload2) {
				        	echo "<script>alert('Gagal Mengupload file atau size file terlalu besar')</script>";
							echo "<script>alert('".$this->upload->display_errors()."')</script>";
							redirect("Admin/produk","refresh");
				       }
					}
		}
	}
		
				
public function simpanfitur()
{
	$this->model_scurity->getscurity();
	$user = $this->session->userdata('username');
	$id = $_POST['id'];


if(isset($_POST ['register'])) {
		$this->form_validation->set_rules('fitur', 'fitur', 'required');
error_reporting(0);
		//jika validasi ok
		if ($this->form_validation->run()== true) {
			echo "<script> alert('Sukses'); </script>";

		// FORM OK
		$data = array(
			'id_fitur' =>time() ,
			'id_produk' =>$_POST['id'] ,
			'fitur' =>$_POST['fitur'] ,
			'status' =>$_POST['status'] ,
		

			 );
			$this->db->insert('fitur',$data);
			$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
			redirect("Admin/produk/fitur/$id","refresh");
		}
	}
}
public function simpanspes()
{
	$this->model_scurity->getscurity();
	$user = $this->session->userdata('username');
	$id = $_POST['id'];


if(isset($_POST ['register'])) {
		$this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'required');
error_reporting(0);
		//jika validasi ok
		if ($this->form_validation->run()== true) {
			echo "<script> alert('Sukses'); </script>";

		// FORM OK
		$data = array(
			'id_spesifikasi' =>time() ,
			'id_produk' =>$_POST['id'] ,
			'menu_spesifikasi' =>$_POST['spesifikasi'] ,
			'spesifikasi' =>$_POST['keterangan'] ,
			'status' =>$_POST['status'] ,
		

			 );
			$this->db->insert('spesifikasi',$data);
			$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
			redirect("Admin/produk/spesifikasi/$id","refresh");
		}
	}
}
public function simpankelebihan()
{
	$this->model_scurity->getscurity();
	$user = $this->session->userdata('username');
	$id = $_POST['id'];


if(isset($_POST ['register'])) {
		$this->form_validation->set_rules('kelebihan', 'kelebihan', 'required');
error_reporting(0);
		//jika validasi ok
		if ($this->form_validation->run()== true) {
			echo "<script> alert('Sukses'); </script>";

		// FORM OK
		$data = array(
			'id_kelebihan' =>time() ,
			'id_produk' =>$_POST['id'] ,
			'kelebihan' =>$_POST['kelebihan'] ,
		

			 );
			$this->db->insert('kelebihan',$data);
			$this->session->set_flashdata("SUCCESS","Akun ada berhasil di buat");
			redirect("Admin/produk/kelebihan/$id","refresh");
		}
	}
}

	public function hapus()
	{	
$level = $this->session->userdata('level');
		
		if($level== 'Admin'){			
		$this->model_scurity->getscurity();
		$key = $this->uri->segment(4);

		$this->db->where('id_produk',$key);		
		$this->db->delete('produk');
		$query = $this->db->get('produk');
		echo "<script> alert('Sukses'); </script>";
		redirect("Admin/produk","refresh");


		}else{

		// echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
			redirect("admin/produk","refresh");
		}	
	}
	public function hapusfitur()
	{	
		$level = $this->session->userdata('level');
		
		if($level== 'Admin'){			
		$this->model_scurity->getscurity();
		$key = $this->uri->segment(4);
		$kuy = $this->uri->segment(5);

		$this->db->where('id_fitur',$key);		
		$this->db->delete('fitur');
		$query = $this->db->get('fitur');
		echo "<script> alert('Sukses'); </script>";
		redirect("Admin/produk/fitur/$kuy","refresh");


		}else{

		// echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
			redirect("admin/produk","refresh");
		}	
	}
		public function hapusspes()
	{	
		$level = $this->session->userdata('level');
		
		if($level== 'Admin'){			
		$this->model_scurity->getscurity();
		$key = $this->uri->segment(4);
		$kuy = $this->uri->segment(5);

		$this->db->where('id_spesifikasi',$key);		
		$this->db->delete('spesifikasi');
		$query = $this->db->get('spesifikasi');
		echo "<script> alert('Sukses'); </script>";
		redirect("Admin/produk/spesifikasi/$kuy","refresh");


		}else{

		// echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
			redirect("admin/produk","refresh");
		}	
	}	
	public function hapuskelebihan()
	{	
		$level = $this->session->userdata('level');
		
		if($level== 'Admin'){			
		$this->model_scurity->getscurity();
		$key = $this->uri->segment(4);
		$kuy = $this->uri->segment(5);

		$this->db->where('id_kelebihan',$key);		
		$this->db->delete('kelebihan');
		$query = $this->db->get('kelebihan');
		echo "<script> alert('Sukses'); </script>";
		redirect("Admin/produk/kelebihan/$kuy","refresh");


		}else{

		// echo "<script> alert('Maaf , Data tidak Kami Temukan'); </script>";
			redirect("admin/produk","refresh");
		}	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */