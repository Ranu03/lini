<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userAdmin extends CI_Model {

	function faktur($id)
	{
		$this->db->where('id_faktur',$id);
		return $this->db->get('faktur');
	}
	public function test()
	{
			$data = "SELECT * from testimoni ";
	       return $this->db->query($data);

	}   
	public function tampiltabel()
    {
       return $this->db->query("show tables")->result();
    }

	
	// filter produk
	// new produk
	function baru(){
		$this->db->limit(2);
		$this->db->order_by('id_produk','desc');
		return $this->db->get('produk');
	}
	
	// lokal
	function lokal(){
		$this->db->where('jenis_produk','Lokal');
		return $this->db->get('produk');
	}

	function produkLokal($perpage,$offset){
		$this->db->where('jenis_produk','Lokal');
		return $this->db->get('produk',$perpage,$offset);
	}

	
}
