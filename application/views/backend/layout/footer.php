  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.1.0
    </div>
    <strong>Copyright &copy; <?= date('Y')?> <a href="#">IT PT. LIMA INTI SINERGI</a></strong>
  </footer>
