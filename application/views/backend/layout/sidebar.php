<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url() ?>asset/admin/dist/img/avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('username');?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?= base_url()?>Admin/home"><i class="fa fa-home"></i> <span>Beranda</span></a></li>

        <?php if ($this->session->userdata('level') == 'User') {?>
 
        <li>
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>My Job</span>
            <span class="pull-right-container">       
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-file"></i> <span>My Document</span>
            <span class="pull-right-container">       
            </span>
          </a>
        </li>
      <?php }elseif ($this->session->userdata('level') == 'Admin') { ?>
        <li>
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Job</span>
            <span class="pull-right-container">       
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-file"></i> <span>Document</span>
            <span class="pull-right-container">       
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-file"></i> <span>LogFile</span>
            <span class="pull-right-container">       
            </span>
          </a>
        </li>
      <?php }elseif($this->session->userdata('level') == 'Super') {?>
       <li>
        <a href="#">
          <i class="fa fa-list-alt"></i> <span>Job</span>
          <span class="pull-right-container">       
          </span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-file"></i> <span>Document</span>
          <span class="pull-right-container">       
          </span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-file"></i> <span>LogFile</span>
          <span class="pull-right-container">       
          </span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-user"></i> <span>User Configurasi</span>
          <span class="pull-right-container">       
          </span>
        </a>
      </li>
      <li class="header"><i class="fa fa-gear"></i> Setting</li>
      <li><a href="<?= base_url()?>index.php/Admin/slide"><i class="fa fa-circle-o text-blue"></i> <span>Slide</span></a></li>
    <?php } ?>
  </ul>
</section>
<!-- /.sidebar -->
</aside>
