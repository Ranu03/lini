	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	?>
	<!doctype html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>EasyGO - Indonesia</title>
		<meta name="Author" content="DikaOji, Dika Fahrozy, Ranu, Razip">
		<link rel="shortcut icon" href="<?= base_url() ?>asset/material/home.png">
		<?php $this->load->view('source/css') ?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>

<style type="text/css">
	
</style>
	<body>
		<div class="body-inner-content">
			<header class="navbar-standerd nav-item" style="background-color: #202020; height: 50px;padding: 5 0px">

			<div class="container">
			
				<div class="row">
					<div class="col-md-2 col-xs-12">
								<img src="<?= base_url() ?>asset/material/head.png" width="100%" style="float: left"/>
						</div>
						<div class="col-md-6 col-xs-6" style="font-size: 9pt;margin-top: 12px">
								<div class="row">
									<div class="col-sm-4 col-xs-6 garis center">
										<a  style="color: #fff!important" href="<?php echo site_url('Home'); ?>">IKUTI KITA DI FACEBOOK</a>
									</div>
									<div class="col-sm-2 col-xs-6 garis">
										<div class="row">
											<div class="col-sm-6 col-xs-6 center">
												<a  style="color: #fff!important" href="<?php echo site_url('Home/index'); ?>">IND</a>
											</div>
											<div class="col-sm-6 col-xs-6 center">
												<a  style="color: #fff!important" href="<?php echo site_url('Home/index_eng'); ?>">ENG</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<nav class="ts-main-menu" style="margin-left: -100px;margin-top: -5px">
							<!--nav brand end-->
							<div class="nav-menus-wrapper clearfix">
								<!-- nav menu start-->
								<ul class="nav-menu">
									<li>
										<a href="#" style="color: #fff">REQUEST</a>
										<div class="megamenu-panel center" style="left: 750px; width: 255px;height: 180px">
											<div class="row scroll">
												<div class="col-12 col-lg-6">
													<div class="item">
														<div class="ts-post-thumb" style="width: 100px;">
															<a href="#">
															<!-- <a href="<?= site_url('home/satu') ?>/<?= $row->id_produk ?>"> -->
																<img class="img-fluid" src="<?= base_url() ?>asset/images/portfolio/1.jpg" >
															</a>
														</div>
														<div class="post-content">
															<h3 class="post-title">
																<a href="#" style="color: #fff">sad</a>
															</h3>
														</div>
													</div>
												</div>
												<div class="col-12 col-lg-6">
													<div class="item">
														<div class="ts-post-thumb" style="width: 100px">
															<a href="#">
															<!-- <a href="<?= site_url('home/satu') ?>/<?= $row->id_produk ?>"> -->
																<img class="img-fluid" src="<?= base_url() ?>asset/images/portfolio/1.jpg" >
															</a>
														</div>
														<div class="post-content">
															<h3 class="post-title">
																<a href="#" style="color: #fff">sad</a>
															</h3>
														</div>
													</div>
												</div>
												
										</div>
									</li>
									
								</ul>
								<!--nav menu end-->
							</div>
						</nav>
						<!-- nav end-->
						<div class="col-md-3 col-xs-4" style="font-size: 9pt;background-color: #202020;">
							<div class="row">
								<div class="col-sm-6 col-xs-3 center garis" style="margin-top: 12px;">
									<a  style="color: #fff" href="<?php echo site_url('Home/cabang'); ?>"><b>KANTOR CABANG</b></a>
								</div>
								<div class="col-sm-4 col-xs-6 center" style="width: 10px">
							<button class="btn" data-toggle="modal" data-target="#searchmodal" style="height: 5px;background-color: #202020">
							<span aria-hidden="true" class="icon-search"></span>
							</button>						
								</div>
							
						</div>
					</div>
				</div>
			
		</header>

			<?php $this->load->view('source/navbar') ?>
			<div  style="min-height: 50%">
				<?php $this->load->view($content) ?>
			</div>
			<?php $this->load->view('source/foot') ?>
			<!-- /.newslater aend -->
		
		</div>
		<?php $this->load->view('source/js') ?>

	</body>
		<!-- Modal-search -->
		<div class="modal fade" id="searchmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				    </div>
			    	<div class="modal-body">
						<div class="form-group">
						    <div class="input-group">
						     	<input class="form-control" type="" placeholder="Search...">
						    	<div class="input-group-addon"><span aria-hidden="true" class="icon-search"></span></div> 
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal-request-presentasi -->
		<div class="modal fade bs-example-modal-lg" id="req1modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content" style="background-repeat:no-repeat;background: url(<?= base_url('asset/material/produk/bg-prod4.png') ?>);background-size:cover">
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				    </div>
			    	<div class="container">
			    		<div class="row">
				    		<div class="col-sm-6">
				    			<img src="<?= base_url() ?>asset/material/request1.png" width="80px" style="float: left;margin-right: 10px" />
				    			<br>
				    			<h3>
				    				FORM REQUEST
						    			<p>PRESENTATION</p>
						    	</h3>
				    		</div>
				    		<div class="col-sm-6 center">
				    			<img src="<?= base_url() ?>asset/material/head_mod.png" width="200px" style="float: right; margin-top: 10px" />
				    		</div>
				    		<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> First Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Company Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Last Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Bussines Field</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Email</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Quantity Plan</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> PIC Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Time & date Presentation</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> PIC Number Telephone</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Bussines Plan</b></div>
								  <div class="panel-body">
								  	<textarea class="form-control"></textarea>
								  </div>
								</div>
							</div>

							<div class="col-sm-6"></div>
							<div class="col-sm-6 center">
				    			<button type="button" class="btn btn-primary" style=" background-color: blue; margin-top: 20px; border-radius: 36px 36px;"><span aria-hidden="true" class="icon-envelope"></span> Send An Email</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>	
	
		<!-- Modal-request-penawaran -->
		<div class="modal fade bs-example-modal-lg" id="req2modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content" style="background-repeat:no-repeat;background: url(<?= base_url('asset/material/produk/bg-prod4.png') ?>);background-size:cover">
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				    </div>
			    	<div class="container">
			    		<div class="row">
				    		<div class="col-sm-6">
				    			<img src="<?= base_url() ?>asset/material/request1.png" width="80px" style="float: left;margin-right: 10px" />
				    			<br>
				    			<h3>
				    				FORM REQUEST
						    			<p>PENAWARAN</p>
						    	</h3>
				    		</div>
				    		<div class="col-sm-6 center">
				    			<img src="<?= base_url() ?>asset/material/head_mod.png" width="200px" style="float: right; margin-top: 10px" />
				    		</div>
				    		<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> First Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Last Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Email</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Company Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> PIC Name</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Bussines Field</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> PIC Telephone Number</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>
							<div class="col-sm-6">
				    			<div class="panel" style="color: black; background-color: gray;border-radius: 5px 5px; margin-top: 10px">
								  <div class="panel-heading" style="margin-left: 5px"><b> Quantity Plan</b></div>
								  <div class="panel-body">
									<input class="form-control" type="text" placeholder="">
								  </div>
								</div>
							</div>

							<div class="col-sm-6"></div>
							<div class="col-sm-6 center">
				    			<button type="button" class="btn btn-primary" style=" background-color: blue; margin-top: 20px; border-radius: 36px 36px;"><span aria-hidden="true" class="icon-envelope"></span> Send An Email</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>				

	</html>