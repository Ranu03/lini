<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>EasyGO - Indonesia</title>
	<meta name="Author" content="DikaOji, Dika Fahrozy, Ranu, Razip">
	<link rel="shortcut icon" href="<?= base_url() ?>asset/material/home.png">
	
	<?php $this->load->view('source/css') ?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body>
	<div class="body-inner-content">
		<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-2 col-xs-12">
							<img src="<?= base_url() ?>asset/material/head.png" width="100%" style="float: left"/>
						</div>
						<div class="col-md-6 col-xs-6" style="font-size: 9pt;margin-top: 12px">
							<div class="row">
								<div class="col-sm-4 col-xs-6 garis center">
									<a  style="color: #fff!important" href="<?php echo site_url('Home'); ?>">FOLLOW US ON FACEBOOK</a>
								</div>
								<div class="col-sm-2 col-xs-6 garis">
									<div class="row">
										<div class="col-sm-6 col-xs-6 center">
											<a  style="color: #fff!important" href="<?php echo site_url('Home/index'); ?>">IND</a>
										</div>
										<div class="col-sm-6 col-xs-6 center">
											<a  style="color: #fff!important" href="<?php echo site_url('Home/index_eng'); ?>">ENG</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6" style="margin-top: 7px">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Search.." type="text" value="" style="color: black" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search" title="Click to start searching"></span>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<?php $this->load->view('source/navbar_eng') ?>
		<?php $this->load->view($content) ?>
		<?php $this->load->view('source/foot_eng') ?>
		<!-- /.newslater aend -->
	</div>

	<?php $this->load->view('source/js') ?>

</body>

</html>