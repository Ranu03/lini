<!-- CSS
   ==================================================== -->
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/font-awesome.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/animate.css">
	<!-- scroll bar -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/jquery.mCustomScrollbar.css">
	<!-- IcoFonts -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/icofonts.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>asset/owlcarousel/owl.theme.default.min.css">
	<!-- slick -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/slick.css">
	<!-- navigation -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/navigation.css">
	<!-- magnific popup -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/magnific-popup.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/stylee.css">
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/custom.css">
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/color-14.css">
	<!-- Responsive -->
	<link rel="stylesheet" href="<?= base_url() ?>asset/css/responsive.css">
	<!-- searching -->
	<link href="<?= base_url() ?>asset/css/flexslider.css" rel="stylesheet" media="screen" />
	<link href="<?= base_url() ?>asset/css/cubeportfolio.min.css" rel="stylesheet" />
	<style type="text/css">
		.row{
			margin-bottom: 10px
		}
		.orange{
			color: #f58735;
		}
		.martop{
			margin-top: 60px
		}
		.martom{
			margin-bottom: 50px
		}
		.garis{
			border-left: 1px solid #ABABAB;
		}
		.link > a{
			color: #fff;margin-bottom: 10px
		}
		.link{
			border-bottom: 1px solid;
			padding-bottom: 5px;
			padding-left: 0px;
			padding-top: 5px;
		}
		.center{
			text-align: center
		}
		a:hover{
			color:#eb3400!important;
		}

	</style>
	<!-- Start of kataembah Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=f179e42b-dd48-44e6-b046-643067afaac9"> </script>
<!-- End of kataembah Zendesk Widget script -->