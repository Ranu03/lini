<!-- newslater start -->
<section class="container-fluid" style="background: #3e4045;margin-top: 20px">
	<div class="row" style="margin-top: 40px;margin-bottom: 0px;color: #fff">
		<div class="col-sm-1"></div>
		<div class="col-sm-2">
			<h4>CONTACT US</h4>
			<p>
				PT. EasyGo Indonesia <br>
				Jl. Parang Tritis Raya <br>
				Komplek Indo Ruko Lodan No. 1AB <br>
				Jakarta Utara - Indonesia
			</p>
			<p>
				T. 021-698 300 38 <br>
				F. 021-698 300 39 <br>
				M. 081315507667 (Product Info) <br>
				M. 08158806116 (Customer Service) <br>
				cse@easygo.co.id
			</p>
		</div>
		<div class="col-sm-2">
			<h4>OUR SERVICE</h4>			
			<div class="col-sm-12 link">
				<a href="#">GPS Tracking</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">Server Dokter GPS</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">IT Solution</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">Web Developer</a>
			</div>
		</div>
		<div class="col-sm-2">
			<h4>CALL CENTER</h4>
			<img src="<?= base_url() ?>asset/material/customerservice.png" alt="" width="60%">
		</div>
		<div class="col-sm-2">
			<h4>COMPANY</h4>
			<div class="col-sm-12 link">
				<a href="#">Management Profile</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">Career</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">Our Blog</a>
			</div>
			<div class="col-sm-12 link">
				<a href="#">Order Online</a>
			</div>
		</div>
		<div class="col-sm-2">
			<h4>LIVE CHAT</h4>
			<img src="<?= base_url() ?>asset/material/chat.png" alt="" width="60%">
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-1"></div>
		<div class="col-sm-10 col-sm-offset-4">
			<hr style="background: #fff">
			<p>© 2018 PT. EasyGo Indonesia</p>
			<ul class="top-social center">
				<li>
					<a href="#">
						<i class="fa fa-twitter"></i>
					</a>
					<a href="#">
						<i class="fa fa-facebook"></i>
					</a>
					<a href="#">
						<i class="fa fa-google-plus"></i>
					</a>
					<a href="#">
						<i class="fa fa-pinterest"></i>
					</a>
					<a href="#">
						<i class="fa fa-vimeo-square"></i>
					</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-1"></div>
	</div>
</section>