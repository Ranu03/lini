<!-- javaScript Files
	=============================================================================-->

	<!-- initialize jQuery Library -->
	<script src="<?= base_url() ?>asset/js/jquery.min.js"></script>
	<!-- navigation JS -->
	<script src="<?= base_url() ?>asset/js/navigation.js"></script>
	<!-- Popper JS -->
	<script src="<?= base_url() ?>asset/js/popper.min.js"></script>
	<!-- magnific popup JS -->
	<script src="<?= base_url() ?>asset/js/jquery.magnific-popup.min.js"></script>
	<!-- Bootstrap jQuery -->
	<script src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
	<!-- Owl Carousel -->
	<script src="<?= base_url() ?>asset/owlcarousel/owl.carousel.min.js"></script>
	<script src="<?= base_url() ?>asset/owlcarousel/query.js"></script>
	<!-- slick -->
	<script src="<?= base_url() ?>asset/js/slick.min.js"></script>
	<!-- smooth scroling -->
	<script src="<?= base_url() ?>asset/js/smoothscroll.js"></script>
	<!-- scroll bar-->
	<!-- <script src="<?= base_url() ?>asset/js/jquery.mCustomScrollbar.concat.min.js"></script> -->
	<script src="<?= base_url() ?>asset/js/main.js"></script>
	<!-- search -->
	<script src="<?= base_url() ?>asset/js/jquery.flexslider-min.js"></script>
	<script src="<?= base_url() ?>asset/js/flexslider.config.js"></script>
	<script src="<?= base_url() ?>asset/js/jquery.appear.js"></script>
	<script src="<?= base_url() ?>asset/js/classie.js"></script>
	<script src="<?= base_url() ?>asset/js/uisearch.js"></script>
	<script src="<?= base_url() ?>asset/js/jquery.cubeportfolio.min.js"></script>
	<script src="<?= base_url() ?>asset/js/custom.js"></script>