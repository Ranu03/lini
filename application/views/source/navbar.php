<?php 
	$uri2 = $this->uri->segment(2);
 ?>
<!-- header nav start-->
		<header class="navbar-standerd nav-item">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!--nav top end-->
						<nav class="navigation ts-main-menu navigation-landscape">
							<div class="nav-header">
								<a class="nav-brand" href="<?= base_url() ?>">
									<img src="<?= base_url() ?>asset/material/home.png" alt="EasyGO Indonesia" style="width: 58px;/*margin-top: 7px;*/">
								</a>
								<div class="nav-toggle"></div>
							</div>
							<!--nav brand end-->
							<div class="nav-menus-wrapper clearfix">
								<!-- nav menu start-->
								<ul class="nav-menu align-to-right">
									<li class="<?= ($uri2 == 'index' || $uri2 == 'home' || $uri2 == '') ? 'active' : '' ?>">
										<a href="<?= base_url() ?>">BERANDA</a>
									</li>
									<li class="<?= ($uri2 == 'produk') ? 'active' : '' ?>">
										<a href="#">PRODUK</a>
										<div class="megamenu-panel center">
											<div class="row scroll">
												 <?php
										          $no =  1 ;
										          foreach ($produk->result() as $row) { 
										            ?>
												<div class="col-12 col-lg-3">
													<div class="item">
														<div class="ts-post-thumb">
															<a href="<?= base_url('home/produk') ?>/<?= $row->id_produk ?>">
															<!-- <a href="<?= site_url('home/satu') ?>/<?= $row->id_produk ?>"> -->
																<img class="img-fluid" src="<?= base_url() ?>asset/images/portfolio/<?= ($row->img == NULL) ? '1.jpg' : $row->img ?>"  alt="<?= $row->nama_produk ?>">
															</a>
														</div>
														<div class="post-content">
															<h3 class="post-title">
																<a href="<?=base_url('home/produk') ?>/<?= $row->id_produk ?>"><?php echo $row->nama_produk ?></a>
															</h3>
														</div>
													</div>
												</div>
												<?php } ?>
												
											</div>
										</div>
									</li>
									<li  class="<?= ($uri2 == 'aplikasi') ? 'active' : '' ?>">
										<a href="https://play.google.com/store/apps/details?id=com.easygovts.easygo" target="_blank">APLIKASI</a>
									</li>
									<li class="<?= ($uri2 == 'promo') ? 'active' : '' ?>">
										<a href="#">PROMO</a>
									</li>
									<li class="<?= ($uri2 == 'solusi') ? 'active' : '' ?>">
										<a href="#">SOLUSI</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= base_url('Home/solusi') ?>">EasyGo Solusion</a>
											</li>
											<li>
												<a href="<?= base_url('Home/solusi_b') ?>">Bussines Case Client</a>
											</li>
											<li>
												<a href="<?= base_url('Home/solusi_d') ?>">Dokter GPS</a>
											</li>
										</ul>
									</li>
									<li class="<?= ($uri2 == 'panduan') ? 'active' : '' ?>">
										<a href="#">PANDUAN PENGGUNA</a>
									</li>
									<li class="<?= ($uri2 == 'qa') ? 'active' : '' ?>">
										<a href="#">Q&A</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= site_url('home/qa') ?>">Q&A</a>
											</li>
											<li>
												<a href="<?= site_url('home/telegram') ?>">Telegram Tutorial</a>
											</li>
											<li>
												<a href="<?= site_url('home/manual') ?>">Buku Manual</a>
											</li>
											<li>
												<a href="<?= site_url('home/video') ?>">Video Tutorial</a>
											</li>
										</ul>
									</li>
									<li class="<?= ($uri2 == 'tentang') ? 'active' : '' ?>">
										<a href="#">TENTANG KAMI</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= site_url('home/tentang') ?>">Tentang EasyGO</a>
											</li>
											<!-- <li>
												<a href="#">Karir</a>
											</li> -->
											<li>
												<a href="<?= site_url('home/album') ?>">Album EasyGo</a>
											</li>
										</ul>
									</li>
								</ul>
								<!--nav menu end-->
							</div>
						</nav>
						<!-- nav end-->
					</div>
				</div>
			</div>
		</header>
		<!-- header nav end-->