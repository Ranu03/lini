<?php 
	$uri2 = $this->uri->segment(2);
 ?>
<!-- header nav start-->
		<header class="navbar-standerd nav-item">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!--nav top end-->
						<nav class="navigation ts-main-menu navigation-landscape">
							<div class="nav-header">
								<a class="nav-brand" href="<?= base_url() ?>">
									<img src="<?= base_url() ?>asset/material/home.png" alt="EasyGO Indonesia" style="width: 44px;margin-top: 7px;">
								</a>
								<div class="nav-toggle"></div>
							</div>
							<!--nav brand end-->
							<div class="nav-menus-wrapper clearfix">
								<!-- nav menu start-->
								<ul class="nav-menu align-to-right">
									<li class="<?= ($uri2 == 'index' || $uri2 == 'home' || $uri2 == '') ? 'active' : '' ?>">
										<a href="<?= base_url('Home/index_eng') ?>">HOME</a>
									</li>
									<li class="<?= ($uri2 == 'produk') ? 'active' : '' ?>">
										<a href="#">PRODUK</a>
										<div class="megamenu-panel center">
											<div class="row scroll">
												 <?php
										          $no =  1 ;
										          foreach ($produk->result() as $row) { 
										            ?>
												<div class="col-12 col-lg-3">
													<div class="item">
														<div class="ts-post-thumb">
															<a href="<?= site_url('home/satu') ?>">
																<img class="img-fluid" src="<?= base_url() ?>asset/images/portfolio/<?= ($row->img == NULL) ? '1.jpg' : $row->img ?>"  alt="<?= $row->nama_produk ?>">
															</a>
														</div>
														<div class="post-content">
															<h3 class="post-title">
																<a href="<?= site_url('home/produk') ?>/<?= $row->id_produk ?>"><?php echo $row->nama_produk ?></a>
															</h3>
														</div>
													</div>
												</div>
												<?php } ?>
												
											</div>
										</div>
									</li>
									<li  class="<?= ($uri2 == 'aplikasi') ? 'active' : '' ?>">
										<a href="https://play.google.com/store/apps/details?id=com.easygovts.easygo" target="_blank">APPS</a>
									</li>
									<li class="<?= ($uri2 == 'promo') ? 'active' : '' ?>">
										<a href="#">PROMO</a>
									</li>
									<li class="<?= ($uri2 == 'solusi') ? 'active' : '' ?>">
										<a href="#"></span>SOLUTION</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= base_url('Home/solusi_eng') ?>">EasyGo Solution</a>
											</li>
											<li>
												<a href="<?= base_url('Home/solusi_b_eng') ?>">Bussines Case Client</a>
											</li>
											<li>
												<a href="<?= base_url('Home/solusi_d_eng') ?>">Doctor GPS</a>
											</li>
										</ul>
									</li>
									<li class="<?= ($uri2 == 'panduan') ? 'active' : '' ?>">
										<a href="#">USER GUIDE</a>
									</li>
									<li class="<?= ($uri2 == 'qa_en' || $uri2 == 'tele_en' || $uri2 == 'manual' || $uri2 == 'video') ? 'active' : '' ?>">
										<a href="#">Q&A</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= site_url('home/qa_en') ?>">Q&A</a>
											</li>
											<li>
												<a href="<?= site_url('home/tele_en') ?>">Telegram Tutorial</a>
											</li>
											<li>
												<a href="<?= site_url('home/manual_en') ?>">Manual Book</a>
											</li>
											<li>
												<a href="<?= site_url('home/video_en') ?>">Tutorial Video</a>
											</li>
										</ul>
									</li>
									<li class="<?= ($uri2 == 'about' || $uri2 == 'galery') ? 'active' : '' ?>">
										<a href="#">ABOUT US</a>
										<ul class="nav-dropdown">
											<li>
												<a href="<?= site_url('home/about') ?>">About EasyGO</a>
											</li>
											<li>
												<a href="<?= site_url('home/galery') ?>">EasyGo Galery</a>
											</li>
										</ul>
									</li>
								</ul>
								<!--nav menu end-->
							</div>
						</nav>
						<!-- nav end-->
					</div>
				</div>
			</div>
		</header>
		<!-- header nav end-->