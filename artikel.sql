-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 08:12 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easygo`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` varchar(20) NOT NULL,
  `title` varchar(30) NOT NULL,
  `artikel` text,
  `img` text,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `title`, `artikel`, `img`, `date`) VALUES
('1547380818', 'U-Box 6 GPS dan Quad Band GSM ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis\r\n voluptate placeat quasi tenetur tempora porro facilis perferendis \r\neveniet quo quam velit quos hic recusandae, veritatis temporibus animi \r\nearum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit', 'admin1547313705.png', NULL),
('1547380841', 'GPS+ Dua GPS Modul Tracking', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis\r\n voluptate placeat quasi tenetur tempora porro facilis perferendis \r\neveniet quo quam velit quos hic recusandae, veritatis temporibus animi \r\nearum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit', NULL, NULL),
('1547380890', 'Melacakwaktu / Jarak interval', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis\r\n voluptate placeat quasi tenetur tempora porro facilis perferendis \r\neveniet quo quam velit quos hic recusandae, veritatis temporibus animi \r\nearum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit', NULL, NULL),
('1547380921', 'Menambah Keamanan kendaraan te', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis\r\n voluptate placeat quasi tenetur tempora porro facilis perferendis \r\neveniet quo quam velit quos hic recusandae, veritatis temporibus animi \r\nearum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
