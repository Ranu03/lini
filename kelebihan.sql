-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2019 at 05:35 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easygo`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelebihan`
--

CREATE TABLE `artikel` (
  `id_artikel` varchar(20) NOT NULL,
  `id_produk` int(9) DEFAULT NULL,
  `title` varchar(30) NOT NULL,
  `artikel` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelebihan`
--

INSERT INTO `artikel` (`id_artikel`, `id_produk`, 'title',`artikel`) VALUES
('1547380818', 1547379103, 'U-Box 6 GPS dan Quad Band GSM MHZ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis
 voluptate placeat quasi tenetur tempora porro facilis perferendis 
eveniet quo quam velit quos hic recusandae, veritatis temporibus animi 
earum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit'),
('1547380841', 1547379103, 'GPS+ Dua GPS Modul Tracking' 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis
 voluptate placeat quasi tenetur tempora porro facilis perferendis 
eveniet quo quam velit quos hic recusandae, veritatis temporibus animi 
earum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit'),
('1547380890', 1547379103, 'Melacakwaktu / Jarak interval' 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis
 voluptate placeat quasi tenetur tempora porro facilis perferendis 
eveniet quo quam velit quos hic recusandae, veritatis temporibus animi 
earum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit'),
('1547380921', 1547379103, 'Menambah Keamanan kendaraan terhitung dari kemungkinan pencurian' 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt nobis
 voluptate placeat quasi tenetur tempora porro facilis perferendis 
eveniet quo quam velit quos hic recusandae, veritatis temporibus animi 
earum, explicabo. ipsum dolor sit amet, consectetur adipisicing elit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelebihan`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
