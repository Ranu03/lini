-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2019 at 05:36 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easygo`
--

-- --------------------------------------------------------

--
-- Table structure for table `spesifikasi`
--

CREATE TABLE `spesifikasi` (
  `id_spesifikasi` varchar(20) NOT NULL,
  `id_produk` int(9) DEFAULT NULL,
  `menu_spesifikasi` varchar(50) NOT NULL,
  `spesifikasi` varchar(50) NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spesifikasi`
--

INSERT INTO `spesifikasi` (`id_spesifikasi`, `id_produk`, `menu_spesifikasi`, `spesifikasi`, `status`) VALUES
('1547393212', 1547379103, 'Dimension ', '96mm*67mm*22.5mm', '0'),
('1547393322', 1547379103, 'Weighy ', '120g', '0'),
('1547393346', 1547379103, 'Oprating Voltage', '10V-30V DC', '0'),
('1547393633', 1547379103, 'Oprating Temperature', '30C - + 70C', '0'),
('1547394452', 1547379103, 'Module', 'SIM800H', '1'),
('1547394660', 1547379103, 'Frequency ', '850/900/1800/1900 MHz GPRS multi-slot class', '1'),
('1547394715', 1547379103, 'GRPS', '10GPRS mobile station class B', '1'),
('1547394808', 1547379103, 'Receiver Sensitivity', '-30C-+70C', '1'),
('1547395012', 1547379103, 'GPS Chipset', 'U-blox NEO 6M GPRS receiver Tracking:-161Bm Cold', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spesifikasi`
--
ALTER TABLE `spesifikasi`
  ADD PRIMARY KEY (`id_spesifikasi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
